<?php
namespace WpCliHooks\WPCLI;

/**
 * Test class
 */
class Hooks  extends \WP_CLI_Command{

/**
 * List registrated functions on hook
 *
 * ## OPTIONS
 *
 * <hook>
 * : The name of action of filter
 * [--plugin=<keyword>]
 * : Filter hooks on plugin
 *
 * [--theme=<keyword>]
 * : Filter hooks on theme
 *
 * ## EXAMPLES
 *
 *     wp hook list init
 *
 * @when shutdown
 */
		public function list($args, $assoc_args){
				list( $hook ) = $args;

				global $wp_filter;
				$list = array();

				if ( ! isset( $wp_filter[ $hook ] ) ) {
					\WP_CLI::error( "No callbacks specified for '{$hook}'." );
				}
				//var_dump($action,array_keys($wp_filter));
				if(isset($wp_filter[$hook])){
					$list = $this->list_actions($wp_filter[$hook]);
				}

				$filter_args = array();
				if(isset($assoc_args['plugin'])){
					$plugin = $assoc_args['plugin'];
					if(!empty($plugin)){
							$filter_args['plugin'] = $plugin;
						}
				}

				if(isset($assoc_args['theme'])){
					$theme = $assoc_args['theme'];
					if(!empty($theme)){
							$filter_args['theme'] = $theme;
						}
				}

				$list = $this->filter_list($list,$filter_args);

				/* Show information */
				if (!empty($list)) {
					if ( empty( $assoc_args['fields'] ) ) {
						$keys = array_keys(current($list));
						$assoc_args['fields'] = $keys;
					}
					$formatter = new \WP_CLI\Formatter($assoc_args);
					$formatter->display_items($list);
				}
		}

		private function filter_list(array $list ,array $args = array()){
			if(empty($args)){
				return $list;
			}

			$args = array_merge(
				array(
					'plugin' => null,
					'theme' => null,
				),$args);

			$set = array();
			foreach ($list as $key => $item) {
				if(!empty($args['plugin'])&&isset($item['path'])&&(\stripos($item['path'],"/plugins/{$args['plugin']}/")===false)){
					continue;
				}

				if(!empty($args['theme'])&&isset($item['path'])&&(\stripos($item['path'],"/themes/{$args['theme']}/")===false)){
					continue;
				}

				$set[] = $item;
			}

			return $set;
		}

		/**
		 * List hooks
		 *
		 * [--search=<keyword>]
		 * : Whether or not to greet the person with success or error.
		 *
		 * ## EXAMPLES
		 *
		 *     wp hook keys
		 *
		 * @when after_wp_load
		 */
		public function keys($args, $assoc_args){
			global $wp_filter;
			$keys = array_keys($wp_filter);

			natsort($keys);

			$search = null;
			if(isset($assoc_args['search'])){
				$search = $assoc_args['search'];
			}

			$list = array();
			if(!empty($keys)){
				foreach($keys as $item){
					//Skip items not containing search key
					if($search!=null&&stripos($item, $search)===false){
						continue;
					}

					$list[] = array(
						'key' => $item
					);
				}
			}

			/* Show information */
			if (!empty($list)) {
				if ( empty( $assoc_args['fields'] ) ) {
					$keys = array_keys(current($list));
					$assoc_args['fields'] = $keys;
				}
				$formatter = new \WP_CLI\Formatter($assoc_args);
				$formatter->display_items($list);
			}
		}


		/**
		 * List actions
		 * @param  \WP_Hook $hook [description]
		 * @return array [description]
		 */
		private function  list_actions(\WP_Hook $hook = null, $args = array()){
			$list = array();

			foreach($hook as $priority => $items){
				foreach($items as $key => $item){
					//base data set
					$record = array(
						'priority' => $priority,
						'key' => $key,
						'class' => '',
						'function' => '',
						'accepted_args' => $item['accepted_args'],
						'path' => '',
					);

					if(is_array($item['function'])){ //Object
						$record['class'] =  is_object($item['function'][0])?get_class($item['function'][0]):$item['function'][0];
						$record['function'] = $item['function'][1];
						$record['path'] = $this->get_object_location($item['function'][0]);
					}else if(is_string($item['function'])){ //Function
						$record['function'] = $item['function'];
						$record['path'] = $this->get_function_location($item['function']);
					}

					$list[] = $record;
				}
			}
			return $list;
		}

		/**
		 * Sanitize path
		 * removes ABSPATH from file path
		 * @param  [type] $path [description]
		 * @return [type]       [description]
		 */
		private function sanitize_path($path){
				$path = str_replace(ABSPATH,'',$path);
				return $path;
		}

		/**
		 * Get Class path
		 * @param  [type] $class [description]
		 * @return [type]        [description]
		 */
		private function get_object_location($class){
			if(is_object($class)){
				$class = get_class($class);
			}

			$reflector = new \ReflectionClass($class);

			$path = $reflector->getFileName();
			$path = $this->sanitize_path($path);
			 //$reflFunc->getStartLine();
      return $path;
		}

		/**
		 * Get function path
		 * @param  [type] $function [description]
		 * @return [type]           [description]
		 */
		private function get_function_location($function){
			$reflFunc = new \ReflectionFunction($function);
			//$reflFunc->getStartLine();
			$path = $reflFunc->getFileName();
			$path = $this->sanitize_path($path);
			return $path;
		}
}
