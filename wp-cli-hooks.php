<?php
/**
 * Class Autoloader
 * @param  string $className Classname
 * @return void
 */
function mu_wpcli_autoloader($className) {
		//Custom names
		$className = str_replace('_', '/', $className);
		//Namespace
		$className = str_replace('\\', '/', $className);
		$className .='.php';

		$path = __DIR__.'/include/classes/'.$className;
		if(file_exists($path)){
			require_once($path);
		}
}

/* Register WPCLI  */
if (defined('WP_CLI') && WP_CLI) {
    spl_autoload_register('mu_wpcli_autoloader');
		/* Register WP CLI commands */
		\WPCLIHooks\WPCLI::register();
}
